"""
Using the ECSOperator to launch our tasks.
"""
from datetime import datetime
from datetime import timedelta
from typing import Any
from typing import Dict

from airflow import DAG
from airflow.contrib.kubernetes.secret import Secret
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.contrib.kubernetes.volume import Volume
from airflow.contrib.kubernetes.volume_mount import VolumeMount

import networkx as nx
import pytz

import sdm.data as sd

DAG_NAME = "edx_events"

secret_volume = Secret(
    "volume",
    # Path where we mount the secret as volume
    "/secrets/env",
    # Name of Kubernetes Secret
    "prod-secrets",
    # Key in the form of service account file name
    "prod-secrets.env",
)

data_volume = Volume(
    name="data", configs={"persistentVolumeClaim": {"claimName": "data-pv-claim"}}
)
data_volume_mount = VolumeMount(
    name="data", mount_path="/data", sub_path=None, read_only=False
)


def create_dag(dag: DAG) -> DAG:

    # Build the network and get the graph.
    G = sd.compose_null_network().G

    # Sort the graph.
    sorted_nodes = list(nx.lexicographical_topological_sort(G))

    # Build the airflow DAG.
    with dag:
        for node in sorted_nodes:
            if "source.edx.events" in node:
                # This gets magically added to the dag...
                args = create_args(node)
                current = KubernetesPodOperator(task_id=node, **args)

                # Loop through the immediate predecessors and set them as upstream
                # tasks
                for predecessor in G.predecessors(node):
                    current.set_upstream(dag.get_task(predecessor))

    return dag


def create_args(node: str) -> Dict[str, Any]:
    return {
        "name": DAG_NAME,
        # Entrypoint of the container, if not specified the Docker container's
        # entrypoint is used. The cmds parameter is templated.
        # cmds=["/venv/bin/sdmcli"],
        "arguments": ["-e", node, "-R", "-y"],
        # The namespace to run within Kubernetes, default namespace is
        # `default`. There is the potential for the resource starvation of
        # Airflow workers and scheduler within the Cloud Composer environment,
        # the recommended solution is to increase the amount of nodes in order
        # to satisfy the computing requirements. Alternatively, launching pods
        # into a custom namespace will stop fighting over resources.
        "namespace": "airflow",
        "secrets": [secret_volume],
        "env_vars": {
            "SDM_ENV_FILE": "/secrets/env/sdm-data.env",
            "AWS_CONFIG_FILE": "/aws/credentials",
        },
        "volumes": [data_volume],
        "volume_mounts": [data_volume_mount],
        # Docker image specified. Defaults to hub.docker.com, but any fully
        # qualified URLs will point to a custom repository. Supports private
        # gcr.io images if the Composer Environment is under the same
        # project-id as the gcr.io images and the service account that Composer
        # uses has permission to access the Google Container Registry
        # (the default service account has permission)
        "image": "sdm-runner:local",
    }


# DAG args
# Create the start date and localise it
start_date = datetime.now() - timedelta(minutes=7)
pytz.timezone("Australia/Perth").localize(start_date)

dag_id = DAG_NAME
default_args = {
    "owner": "LITEC",
    "start_date": start_date,
    "retries": 2,
    "retry_delay": timedelta(minutes=1),
}

schedule = "0 18 * * *"

dag = DAG(dag_id, schedule_interval=schedule, default_args=default_args, catchup=False)

globals()[dag_id] = create_dag(dag=dag)
